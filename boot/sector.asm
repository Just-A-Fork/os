[org 0x7c00]

KERNAL equ 0x1000

mov [BOOT_DRIVE], dl
mov bp, 0x8000
mov sp, bp

mov dx, BOOT16_MSG
call printS
call print_nl

mov bx, KERNAL
mov dh, 2
mov dl, [BOOT_DRIVE]
call disk_load

mov dx, LOADED_MSG
call printS

call switch32
jmp $



[bits 32]
BEGIN_32:

	;mov ebx, MSG_32
	;call print32

	mov dx, [KERNAL]
	call hex2ascii
	and edx, 0xFFFF
	mov ebx, edx
	call print32
	jmp $


%include 'boot/hex2ascii.asm'
%include 'boot/switch32.asm'
%include 'boot/disk_load.asm'
%include 'boot/print32.asm'


BOOT16_MSG db 'Loading Kernal...', 0
LOADED_MSG db 'Kernal Loaded', 0
MSG_32 db 'Booting Kernal...', 0
BOOT_DRIVE db 0

times 510-($-$$) db 0
dw 0xaa55
