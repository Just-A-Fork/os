all: run


bin/sector.bin: boot/sector.asm
	nasm -f bin $< -o $@

kernal/bootstrapper.o: kernal/bootstrapper.asm
	nasm -f elf $< -o $@

kernal/kernal.o: kernal/kernal.c
	i386-elf-gcc -ffreestanding -c $< -o $@

bin/kernal.bin: kernal/kernal.o kernal/bootstrapper.o
	i386-elf-ld -Ttext 0x1000 --oformat binary $^ -o $@

os-image.img: bin/sector.bin bin/kernal.bin
	dd if=/dev/zero of=$@ bs=1024 count=1
	dd if=$< of=$@ conv=notrunc
	dd if=$(word 2,$^) of=$@ bs=1024 seek=1 conv=notrunc

clean: 
	rm os-image.img
	rm bin/*
	rm kernal/*.o

run: os-image.img
	qemu-system-x86_64 -fda $< -d guest_errors -monitor stdio

