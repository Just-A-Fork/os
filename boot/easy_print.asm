
printS:
	pusha

	mov ah, 0x0e

	mov si, dx

printloop:

	lodsb
	
	cmp al, 0
	je end

	int 0x10
	jmp printloop


printSHex:
	pusha
	
	call hex2ascii
	call printS

	popa
	ret
	


end:
	popa
	ret


print_nl:
	pusha
	
	mov ah, 0x0e
	mov al, 0x0a
	int 0x10
	mov al, 0x0d
	int 0x10

	popa
	ret

