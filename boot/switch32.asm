[bits 16]
switch32:
	cli
	lgdt [gdt_descriptor]

	;enable a20
	in al, 0x92
	or al, 2
	and al , ~1
	out 0x92, al

	mov eax, cr0
	or eax, 1
	mov cr0, eax
	jmp CODE_SEG:init32


[bits 32]
init32:
	mov ax, DATA_SEG
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ebp, 0x90000
	mov esp, ebp

	jmp BEGIN_32


%include 'boot/gdt.asm'
